#!/bin/bash
set -Eeuo pipefail

ARCH=$1
MACHINE=$2


# Install packages using APT if not building with clickable (as a library)
if [ -z "${INSTALL_DIR:-}" ]; then
	apt-get update
	apt-get install scons pkg-config libgl1-mesa-dev:$ARCH libglu-dev:$ARCH libasound2-dev:$ARCH libpulse-dev:$ARCH \
			libudev-dev:$ARCH yasm:$ARCH libsdl2-2.0-0:$ARCH libsdl2-dev:$ARCH libudev-dev:$ARCH libudev1:$ARCH \
			libglib2.0-0:$ARCH libglib2.0-dev:$ARCH libegl1-mesa-dev:$ARCH libgles2-mesa-dev:$ARCH libmirclient-dev:$ARCH \
			libxkbcommon-dev:$ARCH libvpx-dev:$ARCH libvpx3:$ARCH libpng16-dev:$ARCH libpng16-16:$ARCH libvorbis-dev:$ARCH \
			libvorbis0a:$ARCH libtheora-dev:$ARCH libtheora0:$ARCH -y
fi

ls /usr/lib/${MACHINE}/pkgconfig

export PKG_CONFIG_PATH="/usr/lib/${MACHINE}/pkgconfig":"/usr/lib/pkgconfig":"/usr/share/pkgconfig"

scons -j4 platform=sailfish target=release builtin_libvpx=no builtin_libtheora=no builtin_libpng=yes CXX=${MACHINE}-g++ CC=${MACHINE}-gcc LINKER=${MACHINE}-ld CCFLAGS=-Wno-attributes
${MACHINE}-strip bin/godot.sailfish*

mkdir -p bin/${ARCH}
cp bin/godot.sailfish* bin/${ARCH}/godot

mv bin/godot.sailfish* godot.ubports.$ARCH
